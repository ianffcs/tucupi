(ns tucupi.core-test
  (:require [tucupi.core :as tucupi]
            [hodur-engine.core :as hodur]
            [datascript.core :as d]
            #?(:clj  [clojure.test :refer [deftest is testing]]
               :cljs [cljs.test :as t :include-macros true])))


(deftest tucupi-schema->type+field->>test
  (let [tucupi1 '{:user/name           String
                  :user/email          Email
                  :email/name          String
                  :email/registered-at DateTime}]
    (is (= (tucupi/tucupi-schema->pt-type+field+pm-type tucupi1)
           [{::tucupi/parent-type    "User"
             ::tucupi/field          "name"
             ::tucupi/primitive-type "String"}
            {::tucupi/parent-type    "User"
             ::tucupi/field          "email"
             ::tucupi/primitive-type "Email"}
            {::tucupi/parent-type    "Email"
             ::tucupi/field          "name"
             ::tucupi/primitive-type "String"}
            {::tucupi/parent-type    "Email",
             ::tucupi/field          "registered-at",
             ::tucupi/primitive-type "DateTime"}]))))

(deftest type->id-test
  (let [meta-db (hodur/init-schema '[User
                                     [^{:type String} name
                                      ^{:type Email} email]
                                     Email
                                     [^{:type String} name
                                      ^{:type DateTime} RegisteredAt]])]
    (is (= ["Boolean"
            "DateTime"
            "Email"
            "Float"
            "ID"
            "Integer"
            "String"
            "User"]
           (sort (keys (tucupi/type->id (d/db meta-db))))))))

(deftest pt-type+field+pm-type->tx-data-test
  (let [conn    (d/create-conn @#'hodur/meta-schema)
        conn0   (tucupi/init-db conn {})
        meta-db (hodur/init-schema '[User
                                     [^{:type String} name
                                      ^{:type Email} email]
                                     Email
                                     [^{:type String} name]])]
    (testing ":field/type"
      (is (= 1 (:field/type
                (second (tucupi/pt-type+field+pm-type->tx-data
                         (d/db conn0) {::tucupi/parent-type    "User"
                                       ::tucupi/field          "name"
                                       ::tucupi/primitive-type "String"})))))
      (is (not (nil?
                (:field/type
                 (second (tucupi/pt-type+field+pm-type->tx-data
                          (d/db conn0)  {::tucupi/parent-type    "User",
                                         ::tucupi/field          "email"
                                         ::tucupi/primitive-type "Email"})))))))
    ;; terminar test de query nesse db
    #_(d/transact! meta-db (tucupi/pt-type+field+pm-type->tx-data
                            (d/db meta-db) {::tucupi/parent-type    "User",
                                            ::tucupi/field          "name",
                                            ::tucupi/primitive-type "String"})))
  )

;; query-db-thiago = query-db-ian
(deftest queries-for-testing
  (let [conn   (d/create-conn @#'hodur/meta-schema)
        conn0  (tucupi/init-db conn {})
        conn1  (-> conn0
                   (d/transact! (concat
                                 (tucupi/pt-type+field+pm-type->tx-data
                                  (d/db conn0) {::tucupi/parent-type    "User"
                                                ::tucupi/field          "name"
                                                ::tucupi/primitive-type "String"})
                                 (tucupi/pt-type+field+pm-type->tx-data
                                  (d/db conn0) {::tucupi/parent-type    "User",
                                                ::tucupi/field          "email"
                                                ::tucupi/primitive-type "Email"})))
                   :db-after
                   d/conn-from-db)
        hodur1 (hodur/init-schema '[User
                                    [^{:type String} name
                                     ^{:type Email} email]
                                    Email
                                    [^{:type String} name
                                     ^{:type DateTime} RegisteredAt]])]
    (testing "Fields in a type."
      (is (= #{["User" "email" "Email"] ["User" "name" "String"]}
             (tucupi/fields-related->type @hodur1 "User")
             (tucupi/fields-related->type @conn1 "User"))))

    (testing "Is Parent?"
      (is (not (tucupi/is-primitive? @hodur1 "User")))
      (is (tucupi/is-primitive? @hodur1 "String")))))

(comment
  (deftest to-datomic
    (let [s (schema->datomic
             '[^{:datomic/tag true}
               default

               ^:interface
               Person
               [^String name]

               Employee
               [^String name
                ^{:type           String
                  :doc            "The very employee number of this employee"
                  :datomic/unique :db.unique/identity}
                number
                ^Float salary
                ^Integer age
                ^DateTime start-date
                ^Employee supervisor
                ^{:type        Employee
                  :cardinality [0 n]
                  :doc         "Has documentation"
                  :deprecation "But also deprecation"}
                co-workers
                ^{:datomic/type :db.type/keyword}
                keyword-type
                ^{:datomic/type :db.type/uri}
                uri-type
                ^{:datomic/type :db.type/double}
                double-type
                ^{:datomic/type :db.type/bigdec
                  :deprecation  "This is deprecated" }
                bigdec-type
                ^EmploymentType employment-type
                ^SearchResult last-search-results]

               ^{:union true}
               SearchResult
               [Employee Person EmploymentType]

               ^{:enum true}
               EmploymentType
               [FULL_TIME
                ^{:doc "Documented enum"}
                PART_TIME]])]
      (is (= [#:db{:ident       :employee/age
                   :valueType   :db.type/long
                   :cardinality :db.cardinality/one}
              #:db{:ident       :employee/bigdec-type
                   :valueType   :db.type/bigdec
                   :cardinality :db.cardinality/one
                   :doc         "DEPRECATION NOTE: This is deprecated"}
              #:db{:ident       :employee/co-workers
                   :valueType   :db.type/ref
                   :cardinality :db.cardinality/many
                   :doc
                   "Has documentation\n\nDEPRECATION NOTE: But also deprecation"}
              #:db{:ident       :employee/double-type
                   :valueType   :db.type/double
                   :cardinality :db.cardinality/one}
              #:db{:ident       :employee/employment-type
                   :valueType   :db.type/ref
                   :cardinality :db.cardinality/one}
              #:db{:ident       :employee/keyword-type
                   :valueType   :db.type/keyword
                   :cardinality :db.cardinality/one}
              #:db{:ident       :employee/last-search-results
                   :valueType   :db.type/ref
                   :cardinality :db.cardinality/one}
              #:db{:ident       :employee/name
                   :valueType   :db.type/string
                   :cardinality :db.cardinality/one}
              #:db{:ident       :employee/number
                   :valueType   :db.type/string
                   :cardinality :db.cardinality/one
                   :unique      :db.unique/identity
                   :doc         "The very employee number of this employee"}
              #:db{:ident       :employee/salary
                   :valueType   :db.type/float
                   :cardinality :db.cardinality/one}
              #:db{:ident       :employee/start-date
                   :valueType   :db.type/instant
                   :cardinality :db.cardinality/one}
              #:db{:ident       :employee/supervisor
                   :valueType   :db.type/ref
                   :cardinality :db.cardinality/one}
              #:db{:ident       :employee/uri-type
                   :valueType   :db.type/uri
                   :cardinality :db.cardinality/one}
              #:db{:ident :employment-type/full-time}
              #:db{:ident :employment-type/part-time
                   :doc   "Documented enum"}]
             s)))))
