(ns tucupi.core
  (:require
   [hodur-engine.core :as hodur]
   [datascript.core :as d]
   [com.wsscode.pathom.connect :as pc]
   [com.wsscode.pathom.core :as p]
   [camel-snake-kebab.core :refer [->PascalCase
                                   ->kebab-case
                                   ->camelCase
                                   ->PascalCaseKeyword
                                   ->kebab-case-keyword
                                   ->camelCaseKeyword
                                   ->snake_case_keyword]]
   [clojure.set :as set]
   [clojure.spec.alpha :as s]))

(comment

  ^{:datomic-tag/recursive true ;; or [...] or false
    }

  '{:user/name      String
    :user/address   Address
    :address/street [Street]
    :street/name    String
    :street/number  Integer}

  '[User
    [^{:type String} name
     ^{:type Address} address]
    Address
    [^{:type        Street
       :cardinality [0 n]} street]
    Street
    [^{:type String} name
     ^{:type Integer} number]]

  '{:user/name            String
    :user/email           Email
    :email/name           String
    :email/resgistered-at DateTime}
  )

(s/def ::tucupi-primitive-type '#{integer string float id})
(s/def ::conn d/conn?)
(s/def ::db d/db?)
(s/def ::tucupi-schema (s/map-of keyword? (s/or :type ::tucupi-primitive-type
                                                :list (s/coll-of ::tucupi-primitive-type))))

(defn init-db [conn schema]
  (-> conn
      (d/transact! (#'hodur/create-primitive-types []))
      :db-after
      d/conn-from-db
      #_#_#_(d/transact! schema)
      :db-after
      d/conn-from-db)
  #_(d/transact! conn (concat (type+field->tx-data "User" "name")
                              (type+field->tx-data "User" "email")))
  ,,,,, ; transacionar aqui o schema do dado
  )

(s/fdef init-db
  :args (s/cat :conn ::conn
               :schema ::tucupi-schema)
  :ret ::conn)

(s/def ::pt-type+field+pm-type
  (s/keys :req [::parent-type
                ::field
                ::primitive-type]))

(defn tucupi-schema->pt-type+field+pm-type
  [schema]
  (reduce-kv
   (fn [coll k v]
     (into coll [{::parent-type    (->PascalCase (namespace k))
                  ::field          (->kebab-case (name k))
                  ::primitive-type (->PascalCase (str v))}])) [] schema))

(s/fdef tucupi-schema->pt-type+field+pm-type
  :args (s/cat :schema ::tucupi-schema)
  :ret (s/coll-of ::pt-type+field+pm-type))

;; types

(defn types
  [db]
  (set
   (map first
        (d/q '[:find ?n
               :where
               [?t :type/name ?n]]
             db))))

(defn type->nature
  [db type]
  (ffirst (d/q '[:find ?n
                 :in $ ?type
                 :where
                 [?t :type/name ?type]
                 [?t :type/nature ?n]] db type)))

;; pode ser primitivo ou qlqr tipo do banco

(defn is-primitive?
  [db type]
  (= :primitive (type->nature db type)))

(s/fdef is-primitive?
  :args (s/cat :db ::db
               :type string?)
  :ret boolean?)

(s/def ::primitive is-primitive?)

(defn fields-related->type [db type]
  (d/q '[:find ?type ?e ?a ;?tn
         :in $ ?type
         :where
         [?e :type/name ?type]
         [?f :field/parent ?e]
         [?f :field/name ?a]
         #_#_#_[?f :field/type ?t]
         [?t :type/name ?tn]
         (not [?t :type/nature :primitive])]
       db type))

(defn type->id [db]
  (into {}
        (d/q '[:find ?name ?e
               :in $
               :where
               [?e :type/name ?name]]
             db)))

(s/fdef type->id
  :args (s/cat :db ::db
               :type string?)
  :ret (s/map-of string? int?))

;; procurar ele no banco, ver se é parent, trata parent, se é primitive
                                        ; como fazer o field-type ser recursivo???!
;; preciso sacar como descer até o field-type existir no banco

;; procura todos os tipos e pega o id, se não tiver id acrescenta o field
;; no banco
;; tools query

#_(hodur/init-schema '[User
                       [^{:type String} name
                        ^{:type Email} email]
                       Email
                       [^{:type String} name
                        ^{:type DateTime} RegisteredAt]])

(defn pt-type+field+pm-type->tx-data
  [db {::keys [parent-type field field-type]}]
  (let [tempid (str (gensym "type"))
        types  (type->id db)]
    ;; quando o tipo for primitivo, o tempid faz abaixo
    ;; quando  o tipo for parent, o tempid tem que estar em outra trx
    [{:type/PascalCaseName  (->PascalCaseKeyword parent-type)
      :type/camelCaseName   (->camelCaseKeyword parent-type)
      :type/kebab-case-name (->kebab-case-keyword parent-type)
      :type/name            (str parent-type)
      :type/snake_case_name (->snake_case_keyword parent-type)
      :type/nature          (->kebab-case-keyword parent-type)
      :node/type            :type
      :db/id                tempid}

     {:field/PascalCaseName  (->PascalCaseKeyword field)
      :field/camelCaseName   (->camelCaseKeyword field)
      #_#_:field/cardinality (->cardinality db parent-type) ;; [1 1]
      :field/kebab-case-name (->kebab-case-keyword field)
      :field/name            (str field)
      :field/parent          tempid
      :field/snake_case_name (->snake_case_keyword field)
      :field/type            (or (get types field-type)
                                 (get types parent-type)
                                 tempid)
      :node/type             :field}

     {:type/PascalCaseName  (->PascalCaseKeyword field-type)
      :type/camelCaseName   (->camelCaseKeyword field-type)
      :type/kebab-case-name (->kebab-case-keyword field-type)
      :type/name            (str field-type)
      :type/snake_case_name (->snake_case_keyword field-type)
      :type/nature          (if (is-primitive? db field-type)
                              :primitive
                              (->kebab-case-keyword field-type))
      :node/type            :type}]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; "email type"                 ;;
;; [8 :field/type 7 536870914]  ;;
;;                              ;;
;;                              ;;
;; "email type"                 ;;
;; [9 :field/type 10 536870913] ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


#_(s/fdef fields-in-a-type
    :args (s/cat :db ::db
                 :type string?))

(comment

  (let []
    (reduce (fn [a i]
              (conj a {:db/id                (get-temp-id! i)
                       :node/type            :type
                       :type/name            (str i)
                       :type/kebab-case-name (->kebab-case-keyword i)
                       :type/camelCaseName   (->camelCaseKeyword i)
                       :type/PascalCaseName  (->PascalCaseKeyword i)
                       :type/snake_case_name (->snake_case_keyword i)
                       :type/nature          :primitive}))
            accum '[String Float Integer Boolean DateTime ID]))

  #_(let [data1     '{:user/name            string
                      :user/email           email
                      :email/name           string
                      :email/resgistered-at date-time}
          #_#_data2 '{:user/name      string
                      :user/address   address
                      :address/street [street]
                      :street/name    string
                      :street/number  integer}
          Types     (schema->Types data1)
          Fields    (schema->Fields data1)
          #_#_db    (deref (hodur/init-schema '[]))
          #_#_conn  (d/conn-from-db db)]
      (->> data1
           (reduce-kv (fn [coll k v]
                        (conj coll
                              [[(namespace k) (name k)] v])) [])
           (group-by ffirst)
           #_(reduce-kv (fn [coll entity [[[_ field] type]]]
                          (conj coll field)
                          #_(conj coll [entity
                                        (with-meta field type)])) [])
           )
      #_(d/transact! conn )))

(defn main! []
  [])
